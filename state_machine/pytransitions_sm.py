from transitions import Machine


class StateMachine:

    states = ['test_logic_reset', 'run_test_idle']

    def __init__(self):
        self.machine = Machine(model=self, states=StateMachine.states, initial='test_logic_reset')

        # you can also use named arguments: trigger, source, dest
        # All transitions can also be defined in a dict or list of lists (more compact)
        self.machine.add_transition('input_0', 'test_logic_reset', 'run_test_idle')
        self.machine.add_transition('input_1', 'test_logic_reset', '=')
        self.machine.add_transition('input_0', 'run_test_idle', '=')
        self.machine.add_transition('input_1', 'run_test_idle', 'test_logic_reset')

    def trigger(self, value):
        if value == 0:
            self.input_0()
        elif value == 1:
            self.input_1()
        else:
            raise NotImplementedError


if __name__ == '__main__':
    TMS = [1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0]
    sm = StateMachine()
    for i in TMS:
        start_state = sm.state
        sm.trigger(i)
        end_state = sm.state
        print(f'Input: {i}, Transition: {start_state} -> {end_state}')
